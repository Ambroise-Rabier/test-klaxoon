import {Bookmark} from '../../bookmark.model';
import {NoEmbed, NoEmbedFlickr, NoEmbedVimeo} from '../../../services/get-no-embed';
import {readableDuration} from '../../../date-utils/readable-duration';
import {memo, useCallback} from 'react';
import {formatDateDDMMMMYYYY} from '../../../date-utils/format-date-DD-MMMM-YYYY';
import {readableDateDiff} from '../../../date-utils/readable-date-diff';

interface Props {
  bookmark: Bookmark;
  onDelete: (url: string) => void;
}

// Design note: This might be slightly incorrect, if there are shortened links
//              like for youtube.com, you can have youtu.be.
//              It assumes no provider has the name of another provider embedded,
//              like youtube-vimeo.com (or as extension youtube.vimeo).
const is = {
  vimeo: (e: any|NoEmbed|{}): e is NoEmbedVimeo => {
    return e?.provider_url?.includes('vimeo');
  },
  flickr: (e: any|NoEmbed|{}): e is NoEmbedFlickr => {
    return e?.provider_url?.includes('flickr');
  }
};

// Note: It could be wrapped with memo, but not worth it for less than 0.1ms
export function BookmarkItem(p: Props) {
  let content;

  if (is.vimeo(p.bookmark.noEmbed)) {
    // Note: noEmbed might seem redundant, but it is needed for typescript
    content = <BookmarkItemVimeo {...p.bookmark} noEmbed={p.bookmark.noEmbed}/>;
  }
  else if (is.flickr(p.bookmark.noEmbed)) {
    content = <BookmarkItemFlickr {...p.bookmark} noEmbed={p.bookmark.noEmbed}/>;
  }
  else {
    content = <a href={p.bookmark.url}>{p.bookmark.url}</a>;
  }

  const onDelete = useCallback(() => {
    if (window.confirm(`Are you sure you want to delete ${p.bookmark.url} ?`)) {
      const p_onDelete = p.onDelete;
      p_onDelete(p.bookmark.url);
    }
  }, [p.onDelete, p.bookmark]);

  return (
    <li data-testid={BookmarkItem.name}>
      {content}
      <button onClick={onDelete}
              data-testid={'BookmarkItem-delete-btn'}>
        Delete
      </button>
    </li>
  );
}


const BookmarkItemVimeo = memo(
  function BookmarkItemVimeo({url, noEmbed, dateAdded}: Bookmark & {noEmbed: NoEmbedVimeo}) {
    return (
      <>
        <h3><a href={url}>{noEmbed.title}</a></h3>
        <a href={url}>{url}</a>
        <a href={url}>
          <img src={noEmbed.thumbnail_url_with_play_button} alt="Play the video"/>
        </a>
        <dl>
          <dt>Author</dt>
          <dd><a href={noEmbed.author_url}>{noEmbed.author_name}</a></dd>

          <dt>Date added</dt>
          <dd>{'Il y a ' + readableDateDiff(dateAdded, new Date())}</dd>

          <dt>Date published</dt>
          <dd>{formatDateDDMMMMYYYY(new Date(noEmbed.upload_date))}</dd>

          <dt>Duration</dt>
          <dd>{readableDuration(noEmbed.duration)}</dd>
        </dl>
      </>
    );
  }
);


const BookmarkItemFlickr = memo(
  function BookmarkItemFlickr({url, noEmbed, dateAdded}: Bookmark & {noEmbed: NoEmbedFlickr}) {
    return (
      <>
        <h3><a href={url}>{noEmbed.title}</a></h3>
        <a href={url}>{url}</a>
        <img src={noEmbed.thumbnail_url}
             alt={noEmbed.title}
             data-testid="thumbnail-flickr"/>
        <dl>
          <dt>Author</dt>
          <dd><a href={noEmbed.author_url}>{noEmbed.author_name}</a></dd>

          <dt>Date added</dt>
          <dd>{'Il y a ' + readableDateDiff(dateAdded, new Date())}</dd>

          <dt>Size</dt>
          <dd>Width: {noEmbed.width} Height: {noEmbed.height}</dd>
        </dl>
      </>
    );
  }
);

