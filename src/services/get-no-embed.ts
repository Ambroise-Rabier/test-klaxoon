/**
 * Might be incomplete, as I use http://json2ts.com/
 * Best would probably be to determine the type reading the source code of the API.
 */
export type NoEmbed = NoEmbedVimeo | NoEmbedFlickr;

export interface NoEmbedVimeo {
  account_type: string;
  url: string;
  thumbnail_height: number;
  thumbnail_url_with_play_button: string;
  author_name: string;
  height: number;
  type: string;
  provider_url: string;
  uri: string;
  upload_date: string;
  thumbnail_width: number;
  video_id: number;
  version: string;
  is_plus: string;
  title: string;
  thumbnail_url: string;
  author_url: string;
  html: string;
  description: string;
  duration: number;
  provider_name: string;
  width: number;
}

export interface NoEmbedFlickr {
  version: string;
  license_url: string;
  license: string;
  web_page: string;
  html: string;
  cache_age: number;
  author_url: string;
  thumbnail_url: string;
  web_page_short_url: string;
  title: string;
  width: number;
  provider_name: string;
  thumbnail_height: number;
  flickr_type: string;
  url: string;
  media_url: string;
  license_id: string;
  provider_url: string;
  height: number;
  type: string;
  author_name: string;
  thumbnail_width: number;
}

export function getNoEmbed(url: string): Promise<NoEmbed> {
  // See https://noembed.com/ for details on the API.
  return fetch(
    `https://noembed.com/embed?url=${url}`
  ).then(
    res => {
      if (res.ok) {
        return res.json();
      } else {
        throw new Error(`Invalid response with status ${res.status}: ${res.text()}`);
      }
    }
  );
}
