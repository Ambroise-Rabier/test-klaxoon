import {readableDateDiff} from './readable-date-diff';

it('now', function () {
  expect(
    readableDateDiff(
      new Date('1995-12-17T03:24:00'),
      new Date('1995-12-17T03:24:00'),
    )
  ).toEqual(
    "à l'instant"
  );
});

it('min', function () {
  expect(
    readableDateDiff(
      new Date('1995-12-17T03:24:00'),
      new Date('1995-12-17T03:22:00'),
    )
  ).toEqual(
    '2 minutes'
  );
});

it('heure', function () {
  expect(
    readableDateDiff(
      new Date('1995-12-17T03:24:00'),
      new Date('1995-12-17T01:24:00'),
    )
  ).toEqual(
    '2 heures'
  );
});

it('jour', function () {
  expect(
    readableDateDiff(
      new Date('1995-12-17T03:24:00'),
      new Date('1995-12-15T03:24:00'),
    )
  ).toEqual(
    '2 jours'
  );
});

it('mois', function () {
  expect(
    readableDateDiff(
      new Date('1995-12-17T03:24:00'),
      new Date('1995-10-12T03:24:00'),
    )
  ).toEqual(
    '2 mois'
  );
});

it('an', function () {
  expect(
    readableDateDiff(
      new Date('1995-12-17T03:24:00'),
      new Date('1993-10-12T03:24:00'),
    )
  ).toEqual(
    '2 ans'
  );
});
