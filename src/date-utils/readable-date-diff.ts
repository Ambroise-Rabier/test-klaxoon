/**
 * Note: french locale.
 */
export function readableDateDiff(previous: Date, current: Date): string {
  const msDiff = Math.abs(current.getTime() - previous.getTime());
  const sDiff = msDiff / 1000;
  const minDiff = sDiff / 60;
  const hDiff = minDiff / 60;
  const dDiff = hDiff / 24;
  const mDiff = dDiff / 31; // approximate
  const yDiff = mDiff / 12; // approximate


  // less than one minute
  if (minDiff < 1) {
    return "à l'instant";
  }

  // less than an hour
  if (hDiff < 1) {
    return `${Math.floor(minDiff)} minute${Math.floor(minDiff) === 1 ? '' : 's'}`;
  }

  // less than a day
  if (dDiff < 1) {
    return `${Math.floor(hDiff)} heure${Math.floor(hDiff) === 1 ? '' : 's'}`;
  }

  // less than a month
  if (mDiff < 1) {
    return `${Math.floor(dDiff)} jour${Math.floor(dDiff) === 1 ? '' : 's'}`;
  }

  // less than a year
  if (yDiff < 1) {
    return `${Math.floor(mDiff)} mois`;
  }

  // more than a year
  if (yDiff >= 1) {
    return `${Math.floor(yDiff)} an${Math.floor(yDiff) === 1 ? '' : 's'}`;
  }

  // should never happen
  console.warn('Unable to parse date in readableDateDiff');

  return '';
}
