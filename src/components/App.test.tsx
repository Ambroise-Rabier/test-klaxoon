import React from 'react';
import {act, render, screen} from '@testing-library/react';
import App from './App';
import userEvent from '@testing-library/user-event';

// Design note: Another way to do it, would be to pass getNoEmbed as props to App (dependency injection)
// noinspection JSUnusedGlobalSymbols
jest.mock('../services/get-no-embed', () => ({
  getNoEmbed: () => Promise.resolve({})
}));

let confirmSpy: jest.SpyInstance;
beforeAll(() => {
  confirmSpy = jest.spyOn(window, 'confirm');
});
afterAll(() => confirmSpy.mockRestore());


it('should render', function () {
  const view = render(<App/>);
  expect(view).toBeTruthy();
});


it('display bookmark url after submit', async function () {
  render(<App/>);

  const url = 'http://localhost:3001/';

  await userEvent.type(screen.getByTestId('input-url'), url);

  // It is needed here, it also removes the need for a waitFor on the next expect.
  // eslint-disable-next-line testing-library/no-unnecessary-act
  await act(async () => {
    await userEvent.click(screen.getByTestId('input-submit'));
  });

  expect(
    screen.getByTestId('BookmarkList')
  ).toBeTruthy();

  expect(
    screen.getByTestId('BookmarkList').textContent
  ).toContain(url);
});


it('display MULTIPLES bookmark url after submit', async function () {
  render(<App/>);

  async function addUrl(url: string) {
    await userEvent.type(screen.getByTestId('input-url'), url);

    // It is needed here, it also removes the need for a waitFor on the next expect.
    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      await userEvent.click(screen.getByTestId('input-submit'));
    });

    expect(
      screen.getByTestId('BookmarkList').textContent
    ).toContain(url);
  }

  const url1 = 'http://localhost:3001/';
  const url2 = 'http://localhost:8000/';

  await addUrl(url1);
  await addUrl(url2);
});


it('can delete bookmark', async function () {
  confirmSpy.mockImplementation(jest.fn(() => true));

  render(<App/>);

  const url = 'http://localhost:3001/';

  await userEvent.type(screen.getByTestId('input-url'), url);

  // It is needed here, it also removes the need for a waitFor on the next expect.
  // eslint-disable-next-line testing-library/no-unnecessary-act
  await act(async () => {
    await userEvent.click(screen.getByTestId('input-submit'));
  });

  await userEvent.click(screen.getByTestId('BookmarkItem-delete-btn'));

  expect(
    screen.getByTestId('BookmarkList').textContent
  ).not.toContain(url);
});

