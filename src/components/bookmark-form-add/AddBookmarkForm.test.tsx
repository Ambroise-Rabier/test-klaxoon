import {render, screen} from '@testing-library/react';
import {AddBookmarkForm} from './AddBookmarkForm';
import userEvent from '@testing-library/user-event';

it('should render', function () {
  const view = render(
    <AddBookmarkForm
      onSubmit={jest.fn()}
    />
  );
  expect(view).toBeTruthy();
});

it('should return url on submit', async function () {
  const fn = jest.fn();
  render(
    <AddBookmarkForm
      onSubmit={fn}
    />
  );
  const url = 'http://localhost:3001/';

  await userEvent.type(screen.getByTestId('input-url'), url);
  await userEvent.click(screen.getByTestId('input-submit'));

  expect(fn).toHaveBeenCalledWith(url);
});

it('should clean input after (valid) submit', async function () {
  const fn = jest.fn();
  render(
    <AddBookmarkForm
      onSubmit={fn}
    />
  );
  const url = 'http://localhost:3001/';

  await userEvent.type(screen.getByTestId('input-url'), url);
  await userEvent.click(screen.getByTestId('input-submit'));

  expect(screen.getByTestId('input-url')).toHaveValue('');
});
