import {getNoEmbed} from './get-no-embed';

let fetchSpy: jest.SpyInstance;
beforeAll(() => {
  fetchSpy = jest.spyOn(window, 'fetch');
});
afterAll(() => fetchSpy.mockRestore());


it('handle code 200 and return NoEmbed', async function () {
  const response = {url: 'http://localhost:3002/'};
  fetchSpy.mockImplementation(
    jest.fn(() => Promise.resolve(new Response(JSON.stringify(response))))
  );

  await expect(
    getNoEmbed('')
  ).resolves.toEqual(response);
});


it('throw error on invalid code 4xx (with JSON body)', async function () {
  fetchSpy.mockImplementation(
    jest.fn(() => Promise.resolve(new Response(JSON.stringify({}), {status: 400})))
  );

  await expect(
    getNoEmbed('')
  ).rejects.toThrow();
});


it('throw error on invalid code 5xx (with JSON body)', async function () {
  fetchSpy.mockImplementation(
    jest.fn(() => Promise.resolve(new Response(JSON.stringify({}), {status: 500})))
  );

  await expect(
    getNoEmbed('')
  ).rejects.toThrow();
});
