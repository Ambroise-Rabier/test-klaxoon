import {getBookmark} from './get-bookmark';
import { getNoEmbed } from './get-no-embed';
import Mock = jest.Mock;

jest.mock('./get-no-embed');


it('should return bookmark', async function () {
  (getNoEmbed as Mock).mockImplementation(
    () => Promise.resolve({title: 'title here'})
  );

  const r = await getBookmark('http://localhost:3001/');

  expect(r.url).toEqual('http://localhost:3001/');
  expect(r.noEmbed).toEqual({title: 'title here'});
  expect(r.dateAdded.constructor.name).toEqual('Date');
});

it('should return bookmark even if get noEmbed error', async function () {
  (getNoEmbed as Mock).mockImplementation(
    () => Promise.reject()
  );

  const r = await getBookmark('http://localhost:3001/');

  expect(r.url).toEqual('http://localhost:3001/');
  expect(r.noEmbed).toEqual({});
  expect(r.dateAdded.constructor.name).toEqual('Date');
});
