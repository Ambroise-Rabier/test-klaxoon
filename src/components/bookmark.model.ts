import {NoEmbed} from '../services/get-no-embed';

// Design note: In case noEmbed doesn't return anything, we still want to save
// the bookmark url, meaning we don't depend on the url field from noEmbed.
export interface Bookmark {
  noEmbed: NoEmbed|{};
  url: string;
  dateAdded: Date;
}
