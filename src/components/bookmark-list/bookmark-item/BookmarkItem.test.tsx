import {render, screen} from '@testing-library/react';
import {BookmarkItem} from './BookmarkItem';
import {NoEmbedFlickr, NoEmbedVimeo} from '../../../services/get-no-embed';
import userEvent from '@testing-library/user-event';

let confirmSpy: jest.SpyInstance;
beforeAll(() => {
  confirmSpy = jest.spyOn(window, 'confirm');

  // Note: don't try to put this line here, it doesn't work.
  // confirmSpy.mockImplementation(jest.fn(() => true));
});
afterAll(() => confirmSpy.mockRestore());

// Note: Wrapping BookmarkItem into `memo()`
//       seems to make screen.getByTestId(BookmarkItem.name)
//       unusable, use view.baseContent instead.
//       It might not be worth using memo on a less than 0.1ms

it('should render', function () {
  const view = render(
    <BookmarkItem
      bookmark={{
        url: 'http://localhost:3001/',
        noEmbed: {} as any,
        dateAdded: new Date('2022-01-01'),
      }}
      onDelete={jest.fn()}
    />
  );
  expect(view).toBeTruthy();
});


it('should render url (even with empty noEmbed)', function () {
  const url = 'http://localhost:3001/';
  const view = render(
    <BookmarkItem
      bookmark={{
        url: url,
        noEmbed: {} as any,
        dateAdded: new Date('2022-01-01'),
      }}
      onDelete={jest.fn()}
    />
  );

  // Note: using screen will not work here,
  expect(
    view.baseElement.textContent
  ).toContain(url);
});


it('should call onDelete when click on delete btn', async function () {
  confirmSpy.mockImplementation(jest.fn(() => true));

  const url = 'http://localhost:3001/';
  const deleteFn = jest.fn();
  render(
    <BookmarkItem
      bookmark={{
        url: url,
        noEmbed: {} as any,
        dateAdded: new Date('2022-01-01'),
      }}
      onDelete={deleteFn}
    />
  );

  await userEvent.click(screen.getByTestId('BookmarkItem-delete-btn'));

  expect(deleteFn).toHaveBeenCalledWith(url);
});

it('should not call onDelete if confirm is false', async function () {
  confirmSpy.mockImplementation(jest.fn(() => false));

  const url = 'http://localhost:3001/';
  const deleteFn = jest.fn();
  render(
    <BookmarkItem
      bookmark={{
        url: url,
        noEmbed: {} as any,
        dateAdded: new Date('2022-01-01'),
      }}
      onDelete={deleteFn}
    />
  );

  await userEvent.click(screen.getByTestId('BookmarkItem-delete-btn'));

  expect(deleteFn).not.toHaveBeenCalledWith(url);
});


// Design note: We could group all expect into one test, but this would make them sequential.
describe('should display Vimeo link attributes', function () {
  jest.useFakeTimers().setSystemTime(new Date('2022-01-01'));

  let vimeoNoEmbed: NoEmbedVimeo;
  const dateAdded = new Date('2020-01-01T03:24:00');
  const dateAddedFormatted = 'Il y a 1 an';  // there is 03:24:00 left to have 2 years ;)
  const datePublishedFormatted = '21 juin 2021'; // 2021-06-21 02:42:24
  const durationFormatted = '00:17:50';// 1070

  function createBookmarkItem() {
    return render(
      <BookmarkItem
        bookmark={{
          url: vimeoNoEmbed.url,
          noEmbed: vimeoNoEmbed,
          dateAdded,
        }}
        onDelete={jest.fn()}
      />
    );
  }

  beforeEach(() => {
    // Design note: recreate the object in case it has been modified by BookmarkItem
    vimeoNoEmbed = {
      "account_type": "pro",
      "url": "https://vimeo.com/565486457",
      "thumbnail_height": 166,
      "thumbnail_url_with_play_button": "https://i.vimeocdn.com/filter/overlay?src0=https%3A%2F%2Fi.vimeocdn.com%2Fvideo%2F1169280957-6513b97be812eac51f6ba090b2f34ab5a63bfc220076c0118950fcf4c227fdce-d_295x166&src1=http%3A%2F%2Ff.vimeocdn.com%2Fp%2Fimages%2Fcrawler_play.png",
      "author_name": "BARTERLINK",
      "height": 240,
      "type": "video",
      "provider_url": "https://vimeo.com/",
      "uri": "/videos/565486457",
      "upload_date": "2021-06-21 02:42:24",
      "thumbnail_width": 295,
      "video_id": 565486457,
      "version": "1.0",
      "is_plus": "0",
      "title": "Sylvain Lhommée @ Nation Entreprenante - Episode #5",
      "thumbnail_url": "https://i.vimeocdn.com/video/1169280957-6513b97be812eac51f6ba090b2f34ab5a63bfc220076c0118950fcf4c227fdce-d_295x166",
      "author_url": "https://vimeo.com/barterlink",
      "html": "<iframe src=\"https://player.vimeo.com/video/565486457?h=e0568b6bc1&amp;app_id=122963\" width=\"426\" height=\"240\" frameborder=\"0\" allow=\"autoplay; fullscreen; picture-in-picture\" allowfullscreen title=\"Sylvain Lhomm&amp;eacute;e @ Nation Entreprenante - Episode #5\"></iframe>",
      "description": "Dans Nation entreprenante, on ne tourne pas autour du pot ! On vous aide à explorer de nouveaux usages pour une économie plus circulaire et solidaire.\n\nIl est possible de se développer sans dépenser, de mutualiser pour évoluer durablement. Et si nous en faisions une valeur ajoutée ?\n\nSylvain Lhommée et Marc Evenisse évoquent ces pratiques ancestrales qui font du bien à notre futur avec Nicolas Celier, notre expert du jour, et Raphaëlle Duchemin, dans ce nouveau numéro de Nation entreprenante, enregistré au Klaxoon Store (Paris 2ème).\n\nDans ce numéro également, Les Poteries d'Albi et Circul'Egg.\n\nNotre expert fil rouge :  Nicolas Celier, Co-fondateur de Ring Capital. Sylvain Lhommée, CEO Fondateur de BarterLink et Marc Evenisse, Fondateur de Furion Motorcycles et VP de Le Mans Tech partagent leur vision sur le sujet.\n\nInvestir ? Fédérer ? Comment faire de chaque crise une chance ?\n\nEt si on relançait l'économie en échangeant une compétence, un savoir-faire, un matériel ou des m2 vides contre un service ou un bien dont on a besoin ? Relocaliser nécessite parfois de mettre en commun des moyens. Humain, financier ou immobilier, nous avons tous un élément précieux à partager.\n\nSylvain Lhommée nous explique comment on peut, grâce au Bartering, préserver sa trésorerie, tout en développant son réseau. Marc Evenisse donne d'autres exemples concrets qui abondent dans le sens du partage et de l'échange.",
      "duration": 1070,
      "provider_name": "Vimeo",
      "width": 426
    };
  });

  it('should display "Aperçu vidéo"', function () {
    // unsure if thumbnail_url or html or thumbnail_url_with_play_button that is asked in spec
    expect(createBookmarkItem().baseElement).toContainHTML(vimeoNoEmbed.thumbnail_url_with_play_button);
  });

  it('should display video title', function () {
    expect(createBookmarkItem().baseElement).toContainHTML(vimeoNoEmbed.title);
  });

  it('should display video author', function () {
    expect(createBookmarkItem().baseElement).toContainHTML(vimeoNoEmbed.author_name);
  });

  it('should display date added formatted', function () {
    expect(createBookmarkItem().baseElement).toContainHTML(dateAddedFormatted);
  });

  it('should display publication date formatted', function () {
    expect(createBookmarkItem().baseElement).toContainHTML(datePublishedFormatted);
  });

  it('should display duration formatted', function () {
    createBookmarkItem();

    expect(createBookmarkItem().baseElement).toContainHTML(durationFormatted);
  });

});


describe('should display Flickr link attributes', function () {
  jest.useFakeTimers().setSystemTime(new Date('2022-01-01'));

  let flickrNoEmbed: NoEmbedFlickr;
  const dateAdded = new Date('2020-01-01T03:24:00');
  const dateAddedFormatted = 'Il y a 1 an';  // compare to above dateAdded and setSystemTime

  function createBookmarkItem() {
    return render(
      <BookmarkItem
        bookmark={{
          url: flickrNoEmbed.url,
          noEmbed: flickrNoEmbed,
          dateAdded,
        }}
        onDelete={jest.fn()}
      />
    );
  }

  beforeEach(() => {
    flickrNoEmbed = {
      "version": "1.0",
      "license_url": "https://creativecommons.org/licenses/by-nc/2.0/",
      "license": "Attribution-NonCommercial License",
      "web_page": "https://www.flickr.com/photos/feuilllu/45771361701/",
      "html": "<a data-flickr-embed=\"true\" href=\"https://www.flickr.com/photos/feuilllu/45771361701/\" title=\"2018 Visite de Klaxoon by Pierre Metivier, on Flickr\"><img alt=\"2018 Visite de Klaxoon\" height=\"685\" src=\"https://noembed.com/i/https://live.staticflickr.com/4817/45771361701_2678123510_b.jpg\" width=\"1024\"></a><script async src=\"https://embedr.flickr.com/assets/client-code.js\" charset=\"utf-8\"></script>",
      "cache_age": 3600,
      "author_url": "https://www.flickr.com/photos/feuilllu/",
      "thumbnail_url": "https://live.staticflickr.com/4817/45771361701_2678123510_q.jpg",
      "web_page_short_url": "https://flic.kr/p/2cJEcbD",
      "title": "2018 Visite de Klaxoon",
      "width": 1024,
      "provider_name": "Flickr",
      "thumbnail_height": 150,
      "flickr_type": "photo",
      "url": "https://www.flickr.com/photos/feuilllu/45771361701/",
      "media_url": "https://live.staticflickr.com/4817/45771361701_2678123510_b.jpg",
      "license_id": "2",
      "provider_url": "https://www.flickr.com/",
      "height": 685,
      "type": "photo",
      "author_name": "Pierre Metivier",
      "thumbnail_width": 150
    };
  });

  it('should display thumbnail', function () {
    expect(
      // Note: Using screen here break the test, for an unknown reason.
      // eslint-disable-next-line testing-library/prefer-screen-queries
      createBookmarkItem().getByTestId('thumbnail-flickr')
    ).toHaveAttribute('src', flickrNoEmbed.thumbnail_url);
  });

  it('should display flickr title', function () {
    expect(createBookmarkItem().baseElement).toContainHTML(flickrNoEmbed.title);
  });

  it('should display flickr author', function () {
    expect(createBookmarkItem().baseElement).toContainHTML(flickrNoEmbed.author_name);
  });

  it('should display date added formatted', function () {
    expect(createBookmarkItem().baseElement).toContainHTML(dateAddedFormatted);
  });

  // no publication date on noEmbed for flickr, see Readme.md

  it('should display width and height', function () {
    createBookmarkItem();

    expect(screen.getByTestId(BookmarkItem.name)).toContainHTML(flickrNoEmbed.width.toString());
    expect(screen.getByTestId(BookmarkItem.name)).toContainHTML(flickrNoEmbed.height.toString());
  });
});
