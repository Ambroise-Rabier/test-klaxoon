import {Bookmark} from '../bookmark.model';
import {BookmarkItem} from './bookmark-item/BookmarkItem';

interface Props {
  bookmarks: Bookmark[];
  onDelete: (url: string) => void;
}

export function BookmarkList(p: Props) {
  return (
    <ol data-testid={BookmarkList.name}>
      {
        p.bookmarks.map(bookmark => <BookmarkItem key={bookmark.url + bookmark.dateAdded.getTime()}
                                                  bookmark={bookmark}
                                                  onDelete={p.onDelete}
        />)
      }
    </ol>
  );
}
