## 1. Install and run

Install the packages with `npm i` and run the app with `npm start`. You can also use `npm build` if you have a webserver
like nginx.

Read [Cra-readme](./CRA-README.md) if you are unfamiliar with CRA.


## 2. Improvement suggestions (what I would mention if this was a real app)

### Duplicated bookmarks

It is missing from the specs, but the user probably don't want duplicates of bookmarks in the list. If this wasn't
a test, I would ask for more information.

As for now, on deletion, it assumes there is no duplicated bookmarks.
But there is nothing hindering you from making duplicated bookmark.
It does make using url as key a bit harder.

### Loading spinner

It might be good to have a loading spinner in case noEmbed take some time to respond.

### Links

The spec mention that I need to display the link, in my opinion, it seems redundant to display
the link when you have thumbnail and the title that is suited to be a clickable link.


## 3. Ambiguous specs

### "Aperçu vidéo"

It is unclear if this reference to the thumbnail or the iframe.

### FR ou ENG ?

L'application est à destination de francophones ou anglophones ? Ce n'est pas le même
texte. Du coup, j'ai mélangé du fr et eng...

### Date de publication sur Flickr

La date n'est pas dans le noEmbed. Le test n'est pas à jour ?
Je pourrais peut-être avoir l'info de la date à travers l'API de Flickr...

