export function readableDuration(duration: number|unknown) {
  if (typeof duration !== 'number') {
    return '-';
  }

  if (duration <= 0) {
    return '00:00:00';
  }

  const d = Math.floor(duration);

  const hours = ((d - d % 3600) / 3600);
  const seconds = (d - hours * 3600) % 60;
  const minutes = (
    (d - hours * 3600 - seconds) / 60
  );

  return (
    `${
      fixLeading0(hours)
    }:${
      fixLeading0(minutes)
    }:${
      fixLeading0(seconds)
    }`
  );
}

function fixLeading0(n: number): string {
  const str = n.toString();

  return str.length === 1 ? '0' + str : str;
}
