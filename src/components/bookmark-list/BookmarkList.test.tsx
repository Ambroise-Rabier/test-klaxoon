import {render, screen} from '@testing-library/react';
import {BookmarkList} from './BookmarkList';


// add twice the same, add a third new, and delete the second.
// both same should be deleted, if the key is just the url
// only one will be deleted.
it('has unique key', function () {
  const view = render(<BookmarkList onDelete={jest.fn()} bookmarks={[
    {
      url: 'https://vimeo.com/565486457',
      dateAdded: new Date('2020-01-01T03:24:01'),
      noEmbed: {},
    },
    {
      url: 'https://vimeo.com/565486457',
      dateAdded: new Date('2020-01-01T03:24:02'),
      noEmbed: {},
    },
    {
      url: 'https://vimeo.com/444',
      dateAdded: new Date('2010-01-01T03:24:03'),
      noEmbed: {},
    },
  ]}/>);

  view.rerender(<BookmarkList onDelete={jest.fn()} bookmarks={[
    {
      url: 'https://vimeo.com/444',
      dateAdded: new Date('2010-01-01T03:24:04'),
      noEmbed: {},
    },
  ]}/>);

  expect(
    screen.getByTestId(BookmarkList.name)
  ).not.toContainHTML('https://vimeo.com/565486457');
});
