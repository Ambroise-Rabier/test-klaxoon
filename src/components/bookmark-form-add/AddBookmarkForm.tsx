import {FormEvent, memo, useCallback} from 'react';
import styles from './AddBookmarkForm.module.css'

interface Props {
  onSubmit: (url: string) => void;
}

export const AddBookmarkForm = memo(
  function AddBookmarkForm(p: Props) {

    // Design note: I prefer to use uncontrolled input here.
    const onSubmit = useCallback((e: FormEvent) => {
      const $form = e.currentTarget as HTMLFormElement;
      const data = new FormData($form);

      const p_onSubmit = p.onSubmit;
      p_onSubmit(data.get('url') as string);

      $form.reset()

      e.preventDefault();
    }, [p.onSubmit]);

    return (
      <form onSubmit={onSubmit}>
        <label>
          <span>Enter an url and submit: </span>
          <input type="url"
                 data-testid="input-url"
                 name="url"
                 className={styles.inputUrl}
                 autoFocus/>
        </label>
        <input type="submit" data-testid="input-submit"/>
      </form>
    );
  }
);
