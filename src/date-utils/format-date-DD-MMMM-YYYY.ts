
// the naming is not terrible, but hey, we would be using DayJS any other day ;)
export function formatDateDDMMMMYYYY(date: Date) {
  return date.toLocaleDateString('fr-FR', {
    year: 'numeric',
    month: 'long',
    day: 'numeric'
  });
}
