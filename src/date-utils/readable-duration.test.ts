import {readableDuration} from './readable-duration';

it('return - if invalid duration', function () {
  expect(
    readableDuration(null)
  ).toEqual(
    '-'
  );
});

it('return 00:00:00 when d inferior or equal to 0', function () {
  expect(
    readableDuration(-1)
  ).toEqual(
    '00:00:00'
  );
  expect(
    readableDuration(0)
  ).toEqual(
    '00:00:00'
  );
});

it('ignore float', function () {
  expect(
    readableDuration(1.1)
  ).toEqual(
    '00:00:01'
  );
});

it('return 00:00:59 when d = 59', function () {
  expect(
    readableDuration(59)
  ).toEqual(
    '00:00:59'
  );
});

it('return 00:01:01 when d = 61', function () {
  expect(
    readableDuration(61)
  ).toEqual(
    '00:01:01'
  );
});

it('return 00:59:59 when d = 3599', function () {
  expect(
    readableDuration(3599)
  ).toEqual(
    '00:59:59'
  );
});

it('return 01:00:00 when d = 3600', function () {
  expect(
    readableDuration(3600)
  ).toEqual(
    '01:00:00'
  );
});

it('return 50:10:04 when d = 3600 * 50 + 10 * 60 + 4', function () {
  expect(
    readableDuration(3600 * 50 + 10 * 60 + 4)
  ).toEqual(
    '50:10:04'
  );
});
