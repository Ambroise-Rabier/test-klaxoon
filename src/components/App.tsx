import React, {useCallback, useState} from 'react';
import './App.css';
import {AddBookmarkForm} from './bookmark-form-add/AddBookmarkForm';
import {BookmarkList} from './bookmark-list/BookmarkList';
import {Bookmark} from './bookmark.model';
import {getBookmark} from '../services/get-bookmark';

function App() {
  const [bookmarks, setBookmarks] = useState<Bookmark[]>([]);

  const onNewBookmark = useCallback(async (url: string) => {
    const newBookmark: Bookmark = await getBookmark(url);

    setBookmarks(prevState => [...prevState, newBookmark]);
  }, []);

  const onDeleteBookmark = useCallback((url: string) => {
    // Design note: This would DELETE ALL BOOKMARKS with
    //              the same URL, not just one.
    //              But as described in Readme.md
    //              Allowing duplicated bookmark is an
    //              OPEN QUESTION.
    setBookmarks(prevState => prevState.filter(e => e.url !== url));
  }, []);

  return (
    <div className="App">
      <AddBookmarkForm onSubmit={onNewBookmark}/>
      <BookmarkList bookmarks={bookmarks}
                    onDelete={onDeleteBookmark}/>
    </div>
  );
}

export default App;
