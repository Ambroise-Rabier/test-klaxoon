import {Bookmark} from '../components/bookmark.model';
import {getNoEmbed} from './get-no-embed';


export async function getBookmark(url: string): Promise<Bookmark> {
  // return an empty object {} when there is an error in the request.
  const noEmbed = await getNoEmbed(url).catch(() => ({}));

  return {
    url,
    noEmbed,
    dateAdded: new Date(),
  };
}
