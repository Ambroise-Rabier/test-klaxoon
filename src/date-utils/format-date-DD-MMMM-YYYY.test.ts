import {formatDateDDMMMMYYYY} from './format-date-DD-MMMM-YYYY';

it('should return 17 novembre 2021', function () {
  expect(
    formatDateDDMMMMYYYY(new Date('2021-11-17T03:24:00'))
  ).toEqual(
    '17 novembre 2021'
  );
});
